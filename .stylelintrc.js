module.exports = {
  processors: [
    [
      '@mapbox/stylelint-processor-arbitrary-tags',
      {
        fileFilterRegex: [/\.vue$/]
      }
    ]
  ],
  extends: [
    'stylelint-config-recommended',
    'stylelint-config-recommended-scss',
    // 'stylelint-prettier/recommended',
  ],
  plugins: ['stylelint-scss'],
  rules: {
    'max-line-length': 80,
    'no-empty-source': null,
    'at-rule-empty-line-before': [
      'always',
      {
        ignore: ['first-nested', 'after-comment'],
        ignoreAtRules: ['return', 'warn', 'import', 'else'],
      },
    ],
    // See https://regex101.com/r/4jSx2O/3 to test the regex, matches the following:
    // .foo
    // .foo.is-bar
    // .foo--bar
    // .foo__baz
    // .foo__baz--bar
    // .foo-bar
    // .foo-bar--foo
    // .foo-bar__baz
    // .foo-bar__baz--foo
    // .foo-bar__bar-foo
    // .foo-bar__bar-foo--baz
    'selector-class-pattern':
      '^(?:(?:o|c|u|t|s|is|has|_|js|qa)-)?[a-z0-9]+(?:-[a-z0-9]+)*(?:__[a-z0-9]+(?:-[a-z0-9]+)*)?(?:--[a-z0-9]+(?:-[a-z0-9]+)*)?(?:\\[.+\\])?$',
  },
}
